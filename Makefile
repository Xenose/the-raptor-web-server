

CC=clang
FLAGS=-Wall
LIBS=-ldl

default:
	$(CC) $(FLAGS) -g src/*.c $(LIBS) -o server.bin

ext:
	$(CC) $(FLAGS) -c -fPIC extensions/src/Lang.c -o extensions/o_files/Lang.o
	$(CC) -shared -lc -o extensions/o_files/Lang.so extensions/o_files/Lang.o
