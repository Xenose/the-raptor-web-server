#include"../../src/Types.h"

uint32_t ExtensionMain(API* api) {

	char* out = "EN";
	StringList* list = api->slist;

	if (NULL != api->file->lang) {
		goto SKIP;
	}
	

SKIP:	
	while (NULL != list->pNext) list = list->pNext;
	
	list->pNext = (StringList*)malloc(sizeof(StringList));
	memset(list->pNext, 0, sizeof(StringList));
	list = list->pNext;

	list->bytes = strlen(out) * sizeof(char);
	list->pData = (char*) malloc(list->bytes);
	list->pNext = NULL;
	memcpy(list->pData, out, list->bytes);

	return list->bytes;
}
