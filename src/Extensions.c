#define _GNU_SOURCE
#include<dlfcn.h>
#include<dirent.h>
#include<string.h>
#include"Extensions.h"
#include"ServerString.h"

int8_t __ExtensionParseConf(Extension* extension, const char* file) {

	StringHandler.ParseValue(&extension->name, "name:", file);
	StringHandler.ParseValue(&extension->type, "type:", file);
	
	extension->pNext = NULL;
	return 0x0;
}

uint8_t __ExtensionLoad(Extensions* extensions, const char* path) {

	char filePath[PATH_MAX] = ""; 
	char* file = NULL;
	int32_t fd = 0;
	int32_t bytes	= strlen(path);
	Extension* extension = NULL;
	Extension* current = NULL;
	void* handle = NULL;

	char* infos		= (char*)malloc(bytes + sizeof("/info") + 1);
	memcpy(infos, path, bytes);
	memcpy(infos + bytes, "/info", sizeof("/info"));

	DIR* iffd	= opendir(infos);
	
	for (struct dirent* i = readdir(iffd); NULL != i; i = readdir(iffd)) {
		
		if (!(DT_REG & i->d_type))
			continue;

		strcpy(filePath, infos);
		strcpy(filePath + strlen(filePath), "/");
		strcpy(filePath + strlen(filePath), i->d_name);

		fd = open(filePath, O_RDONLY);
		bytes = lseek(fd, 0, SEEK_END);
		
		strcpy(filePath, path);
		strcpy(filePath + strlen(filePath), "/o_files/");

		if (0 < bytes) {
			file = mmap(NULL, bytes, PROT_READ, MAP_PRIVATE, fd, 0);
			extension = (Extension*)malloc(sizeof(Extension));
			memset(extension, 0, sizeof(Extension));
			
			extension->name = NULL;
			extension->type = NULL;
			extension->path = NULL;
			
			__ExtensionParseConf(extension, file);

			if (NULL == extension->name) break;
		
			strcpy(filePath + strlen(filePath), extension->name);
			strcpy(filePath + strlen(filePath), ".so");
			
			handle = dlopen(filePath, RTLD_NOW);

			if (NULL == handle) {
				fprintf(stderr, "\t\t\033[31mFailed to load object\033[0m :: %s\n", filePath);
				goto SKIP_FOR_LOOP;
			}

			printf("\t\t\033[32mLoaded extension\033[0m :: %s\n", filePath);

			dlerror();
			extension->Execute = (int32_t (*)(API* api)) dlsym(handle, "ExtensionMain");

			//dlclose(handle);	  


			if (!strcmp(extension->type, "html")) { 

				if (NULL == extensions->html) {
					extensions->html = extension;
					goto SKIP_FOR_LOOP;
				}

				for (current = extensions->html; NULL != current->pNext; current = current->pNext);
				current->pNext = extension;
			}
SKIP_FOR_LOOP:{}
		}
		memset(filePath, 0, PATH_MAX);
	}

	return 0x0;
}

uint8_t __ExtensionUnload(Extension* extension)
{
	return 0x0;
}

const __ExtensionHandler ExtensionHandler = {
	.Load =		&__ExtensionLoad,
};
