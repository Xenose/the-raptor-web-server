#ifndef __extensions_header__
#define __extensions_header__

#include"Types.h"

#define _GNU_SOURCE
#include<dlfcn.h>

typedef struct {
	uint8_t (*Load)(Extensions* extensions, const char* path);
	uint8_t (*Unload)(Extension** extensions);
	char (*Run)(Server* server, const char* extName);
} __ExtensionHandler;

extern const __ExtensionHandler ExtensionHandler;

#endif //__extensions_header__
