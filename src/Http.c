#include<stdlib.h>
#include"Http.h"
#include"ServerString.h"

static uint32_t headerSize = 0;
static char* headerBuffer = NULL;

static const char error400[] =
	"HTTP/1.1 400 Not Found\r\n"
	"Connection: close\r\n"
	"Content-Length: 10\r\n"
	"\r\nNot Found!";

static const char error404[] =
	"HTTP/1.1 404 Not Found\r\n"
	"Connection: close\r\n"
	"Content-Length: 14\r\n"
	"\r\n404 Not Found!";
	
static const char error200[] =
	"HTTP/1.1 200 OK!\r\n"
	"Connection: close\r\n"
	"Content-Type: text/html; charset=UTF-8\r\n"
	"Content-Length: 136\r\n"
	"\r\n";

static const int8_t __HttpParse(const char* text, Http* header) {

	char* tmp = NULL;
	StringHandler.ParseValue(&tmp, "get", text);

	header->file = NULL;

	if (NULL != tmp) {
		StringHandler.CutOff(&header->file, tmp, " ");
		free(tmp);

		puts(header->file);
	}

	return 0;
}

static const char* __HttpCreate(Http* header, const ServerFile* file) {
	uint32_t bytes = 0; 

REDO: /// I know evil goto, but its cleaner then the alternatives and this is not a loop 
	bytes = snprintf(headerBuffer, headerSize,
			"HTTP/1.1 200 OK!\r\n"
			"Connection: close\r\n"
			"Content-Length: %u\r\n"
			"\r\n", 
			file->bytes);

	printf("bytes %u left\n\n", headerSize);

	if (0 != bytes && bytes > headerSize) {
		headerSize += bytes + 1;
		headerBuffer = (char*)realloc(headerBuffer, headerSize);

		if (NULL == headerBuffer) {
			perror("header malloc :: ");
			goto ERROR_EXIT;
		}
		goto REDO;
	}

ERROR_EXIT:
	return headerBuffer;
} 

static const ServerFile* __HttpGetFile(Http* header, const Server* server) {

	char* fileName = header->file;
	ServerFile* out = NULL;

	if (!strcmp(header->file, "/")) {
		fileName = "resources/pages/index.html";
	} else if (fileName[0] == '/') {
		fileName++;
	}

	for (register ServerFile* file = server->files; NULL != file; file = file->pNext) {
		
		if (!strcmp(fileName, file->name)) {
			out = file;
			break;
		}
	}

	return out;
}

static char* __HttpUploadFile(Http* header) {
	return NULL;
}

static const char* __HttpError(int error, uint32_t* bytes) {
	char* out = NULL;
	*bytes = 0;

	switch(error) {
		case 200:
			out = (char*)error200;
			*bytes = sizeof error200 - 1;
		case 400:
			break;
		case 404:
			out = (char*)error404;
			*bytes = sizeof error404 - 1;
			break;
	};

	return out;
}


const struct __HttpHandler HttpHandler = {
	.Error	= &__HttpError,
	.Parse	= &__HttpParse,
	.Create	= &__HttpCreate,
	.GetFile = &__HttpGetFile,
};
