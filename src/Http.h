#ifndef __http_header__
#define __http_header__

#include"Types.h"

#define HTTP_HEADER_SIZE	2048

#define HTTP_TYPE_UNKOWN	0x00
#define HTTP_TYPE_GET		0x01
#define HTTP_TYPE_POST		0x02

typedef struct __Http {
	uint8_t	type;
	char*		file;
	char*		pUserAgent;
	char*		version;
	char**	ppLang;
	uint32_t bytes;
} Http;


struct __HttpHandler {
	const char* (*Error)(int error, uint32_t* bytes);
	const int8_t (*Parse)(const char* text, Http* header);
	const char* (*Create)(Http* header, const ServerFile* file);
	const ServerFile* (*GetFile)(Http* header, const Server* server);
};

extern const struct __HttpHandler HttpHandler;

#endif //__http_header__
