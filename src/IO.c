#include<stdlib.h>
#include<fcntl.h>
#include<sys/mman.h>
#include<linux/limits.h>
#include<string.h>
#include<stdio.h>
#include<dirent.h>
#include<unistd.h>
#include"IO.h"

int32_t printStream = 1;
int32_t errorStream = 2;


int32_t Print(const char* x)
{
	return write(printStream, x, strlen(x));
}

int32_t _PrintError()
{
	int sout = printStream;
	printStream = errorStream;
	
	Print(" [ \033[31mERROR\033[0m ]\n");
	perror("\tMessage ");

	printStream = sout;
	return 1;
}

int32_t CheckPublicFileCount(const char* folderPath) {
	int32_t fileCount = 0;
	DIR* ffd = opendir(folderPath);
	char path[PATH_MAX] = { 0 };


	if (NULL == ffd) {
		perror(" [ \033[31mFAILED\033[0m ] ");
		goto EXIT_ERROR;
	}

	for (struct dirent* i = readdir(ffd); NULL != i; i = readdir(ffd)) {
	
		memset(path, 0, sizeof path);
		strcpy(path, folderPath);
		strcpy(path + strlen(path), "/");

		if (i->d_type & DT_REG) {
			++fileCount;
			printf("\t\t\t\033[32m%s\033[0m\n", i->d_name);
			continue;
		}

		if (!(i->d_type & DT_DIR))
			continue;

		if (!strcmp(i->d_name, "."))
			continue;

		if (!strcmp(i->d_name, ".."))
			continue;

		strcpy(path + strlen(path), i->d_name);
		printf("\t\t%s\n", path);
		fileCount += CheckPublicFileCount(path);
	}


	return fileCount;
EXIT_ERROR:
	return -1;
}

