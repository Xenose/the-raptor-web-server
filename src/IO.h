#ifndef __header_io__
#define __header_io__

#include"Types.h"

#define PrintError(func, go) if (func) {\
	_PrintError(__LINE__, #func, __FILE__);\
	goto go;\
}

extern int32_t printStream;
extern int32_t errorStream;

extern int32_t Print(const char* x);
extern int32_t _PrintError(int32_t line, const char* func, const char* file);

extern int32_t CheckPublicFileCount(const char* folderPath);


#endif //__header_io__
