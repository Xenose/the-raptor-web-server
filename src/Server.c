#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>
#include"Server.h"
#include"IO.h"
#include"Http.h"

static API api = {};
int trueTest = 1;

static int32_t __ServerCreate(Server* server) {
	int error = 0;
	int option = 0;
	struct addrinfo hints, *bindAddress;
	memset(&hints, 0, sizeof hints);

	hints.ai_family		= AF_INET6;
	hints.ai_socktype		= SOCK_STREAM;
	hints.ai_flags			= AI_PASSIVE;

	Print("\tConfiguring server...\n\n");
	printf("\t\tserver IP is set to\t : \033[34m%s\033[0m\n", server->pIP);
	printf("\t\tport number is set to\t : \033[34m%s\033[0m\n", server->pPort);

	if ((error = getaddrinfo(server->pIP, server->pPort, &hints, &bindAddress))) {
		fprintf(stderr, "\t[ \033[31mFAILED\033[0m ] getaddrinfo() %s\n\n", gai_strerror(error));
		goto ERROR_EXIT_FREE_BIND_ADDRESS;
	}

	Print("\n\tCreating socket...\n");
	if (-1 == (server->socket = socket(bindAddress->ai_family, bindAddress->ai_socktype, bindAddress->ai_protocol))) {
		perror("\t[ \033[31mFAILED \033[0m ] ");
		goto ERROR_EXIT;
	}

	if (setsockopt(server->socket, IPPROTO_IPV6, IPV6_V6ONLY, (void*)&option, sizeof(option))) {
		perror("\t[ \033[31mFAILED \033[0m ] ");
		goto ERROR_EXIT;
	}
	
	if (setsockopt(server->socket, SOL_SOCKET, SO_REUSEADDR, &trueTest, sizeof(int))) {
		perror("\t[ \033[31mFAILDED \0330m] ");
	}

	Print("\tBinding address...\n");
	if (bind(server->socket, bindAddress->ai_addr, bindAddress->ai_addrlen)) {
		perror("\t[ \033[31mFAILED \033[0m ] ");
		goto ERROR_EXIT;
	}

	freeaddrinfo(bindAddress);

	Print("\tServer is listning...\n");
	if (listen(server->socket, server->queueSize)) {
		perror("\t[ \033[31mFAILED\033[0m ] ");
		goto ERROR_EXIT;
	}

	api.server = server;
	return  0x0;
ERROR_EXIT:
	close(server->socket);
ERROR_EXIT_FREE_BIND_ADDRESS:
	free(bindAddress);
	return -0x1;
}


/// A wrapper function for the accept function, sockets programming
static int32_t __ServerAccept(Server* server, struct sockaddr_storage* storage, socklen_t* length) {
	int32_t out = accept(server->socket, (struct sockaddr*) storage, length);

	if (-1 == out) {
		perror("accept");
		goto EXIT_ERROR;
	}

EXIT_ERROR:
	return out;
}


static int32_t __ServerParsePage(Server* restrict server, ServerFile* restrict file){

	/// The acctual list object
	StringList* listStart = (StringList*)malloc(sizeof(StringList));
	/// The index object that keeps track of where we are
	StringList* list = listStart;
	/// Start points to the start of the next string block
	register char* start = NULL;
	/// End points to the end of the next string block
	register char* end = NULL;
	/// To parse commands without chancing start or end we have current telling ous where to start reading the command
	register char* current = NULL;
	/// The total amount of bytes we have, so that we can allocate memory for the final file
	register uint32_t bytes = 0;

	memset(listStart, 0, sizeof(StringList));

	for (start = file->pData, end = strstr(start, "!rws"); NULL != end; end = strstr(start, "!rws")) {

		/// Setting current to the next command, we don't need !rws so we skip it also.
		current = end + sizeof("!rws");

		/// allocating new memory for the first block of the documment and copying it.
		bytes += list->bytes = end - start;

		if (0 == list->bytes) continue;

		list->pData = (char*)malloc(sizeof(char) * (list->bytes + 1));
		memcpy(list->pData, start, list->bytes);	

		/// We skip the spaces until we find the acctual command
		while(' ' == *(current + 1)) { 
			current++; 
		}

		/// Setting all the parameters for any extension that may be called.
		end = strstr(current, ";");

		/// If end is null then we can stop looking as we don't know where the command ends
		if (NULL == end)
			break;
		
		api.command		= current;
		api.commandEnd = end;
		api.file			= file;
		api.slist		= list;

		/// searching for the right extension to execute
		for (Extension* i = server->extensions.html; NULL != i; i = i->pNext) {
			if (!strncmp(i->name, current, strlen(i->name))) {
				char c = current[strlen(i->name)];
				if (c == ' ' || c == ';' || c == '=' || c == ':') {
					bytes += i->Execute(&api);
					list = list->pNext;
				}
			}
		}

		list->pNext = (StringList*)malloc(sizeof(StringList));
		list = list->pNext;
		start = end + 1;
	} 


	end = file->pData + file->bytes;
	bytes += list->bytes = end - start;
	list->pData = (char*)malloc(sizeof(char) * (list->bytes + 1));
	memcpy(list->pData, start, list->bytes);

	start = current = malloc(bytes + 1);

	if (NULL == current) {
		goto EXIT;
	}

	for (list = listStart; NULL != list; ) {
		memcpy(current, list->pData, list->bytes);
		current += list->bytes;
		free(list->pData);
		
		list = list->pNext;
		free(listStart);
		listStart = list;
	}

	file->pData = start;
	file->bytes = bytes;
EXIT:
	return 0x0;
}

static int32_t __ServerParsePages(Server* server) {
	//uint32_t bytes = 0;

	for (register ServerFile* file = server->files; NULL != file; file = file->pNext) {

		/// For now we only parse html files
		if (NULL == strstr(file->name, ".html") && NULL == strstr(file->name, ".htm"))
			continue;

		if (0 == file->bytes)
			continue;

		__ServerParsePage(server, file);
	}

	return 0x0;
}


////// Client Functions ////////


static int32_t __ClientAdd(Client** client, const int32_t* socket, struct sockaddr_storage* storage, socklen_t* length) {

	/// If there is no other clients then just skip the while loop
	if (NULL == *client) {
		goto SKIP_SEARCH;
	}

	for (Client* i = *client; NULL != i; i = i->pNext) {
		if (i->socket == *socket) {
			puts("Found socket!!!");
			goto EXIT;
		}
	}

	/// The get the last client we go through them all
	while (NULL != client[0]->pNext) {
		*client = client[0]->pNext;
	}
SKIP_SEARCH:
	*client = (Client*)malloc(sizeof(Client));
	memset(*client, 0, sizeof(Client));
	
	getnameinfo((struct sockaddr*) storage, *length, (*client)->ip, sizeof (*client)->ip, 0, 0, NI_NUMERICHOST);

	client[0]->socket = *socket;
	client[0]->storage = *storage;
	client[0]->storageLength = *length;
	client[0]->pNext = NULL;

EXIT:
	return 0;
}

/* 
	This function will drop the client and remove it from the list.
	*/
static int32_t __ClientDrop(Client** client, Server* server) {
	close(client[0]->socket);

	/// If we can skip the for loop we should do so.
	if (*client == server->clients) {
		server->clients = client[0]->pNext;
		goto EXIT;
	}

	/// Check if there is a match to the client 
	for (Client* current = server->clients; NULL != current; current = current->pNext) {
		if (current->pNext == *client) {
			current->pNext = client[0]->pNext;
			goto EXIT;
		}
	}
	
	fprintf(stderr, " [ Client Drop ] Error could not find the client requested");
	return -0x1;

EXIT:
	/// Cleaing up the struct
	FD_CLR(client[0]->socket, &server->readSocks);
	FD_CLR(client[0]->socket, &server->writeSocks);
	free(*client);
	client = NULL;
	return 0x0;
}

static int32_t __ClientRecv(Client* c) {
	if (0 < c->receivedBytes) {
		return 0;
	}

	c->request[MAX_REQUEST_SIZE] = '\0';
	c->receivedBytes = recv(c->socket, c->request, MAX_REQUEST_SIZE, 0);

	for (register int32_t i = 0; '\0' !=  c->request[i] ; i++) {
		c->request[i] = tolower(c->request[i]);
	} 	

	puts(c->request);
	return 0;
}

static int32_t __ClientSend(Client* c, const Server* server) {
	uint32_t bytes = 0;
	const char* buf = HttpHandler.Error(404, &bytes);
	Http header;
	const ServerFile* file = NULL;
	
	HttpHandler.Parse(c->request, &header);

	if (NULL == header.file) {
		goto SEND_ERROR;
	}

	file = HttpHandler.GetFile(&header, server);

	if (NULL == file) {
		goto SEND_ERROR;
	}

	puts("hello");
	const char* headerBuffer = HttpHandler.Create(&header, file);

	puts(headerBuffer);

	buf = HttpHandler.Error(200, &bytes);
	send(c->socket, headerBuffer, strlen(headerBuffer), 0);
	send(c->socket, file->pData, file->bytes, 0);
	goto EXIT;

SEND_ERROR:
	send(c->socket, buf, bytes, 0);
EXIT:
	return 0;
}

const __ServerHandler ServerHandler = {
	.Create		= &__ServerCreate,
	.Accept		= &__ServerAccept,
	.ParsePages	= &__ServerParsePages,
}; 

const __ClientHandler ClientHandler = {
	.Add		= &__ClientAdd,
	.Drop		= &__ClientDrop,
	.Recv		= &__ClientRecv,
	.Send		= &__ClientSend,
};
