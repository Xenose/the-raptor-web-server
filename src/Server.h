#ifndef __header_server__
#define __header_server__

#include"Types.h"


typedef struct {
	 int32_t (*Create)(Server* server);
	 int32_t (*Accept)(Server* server, struct sockaddr_storage* storage, socklen_t* length);
	 int32_t (*ParsePages)(Server* server);
} __ServerHandler;

typedef struct {
	 int32_t (*Add)(Client** client, const int32_t* socket, struct sockaddr_storage* storage, socklen_t* length);
	 int32_t (*Drop)(Client** client, Server* server);
	 Client  (*Get)(int socket);
	 int32_t (*Recv)(Client* client);
	 int32_t (*Send)(Client* c, const Server* server);
} __ClientHandler;

extern const __ServerHandler ServerHandler;
extern const __ClientHandler ClientHandler;

#endif //__header_server__
