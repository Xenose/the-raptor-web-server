#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include"ServerString.h"

uint32_t __ServerStringParseValue(char** out, const char* parameter, const char* file) {

	uint32_t bytes = 0;
	register char* varible = NULL;
	
	if (NULL == (varible = strstr(file, parameter)))
		goto EXIT_ERROR;

	varible += strlen(parameter);

	if (NULL != out[0]) {
		free(out[0]);
		out[0] = NULL;
	}

	while (*varible == ' ' || *varible == '\t')
		++varible;
	
	bytes = strcspn(varible, "\r\n");

	if (0 >= bytes)
		goto EXIT_ERROR;

	out[0] = (char*)malloc(sizeof(char) * (bytes + 1));
	memset(out[0], 0, bytes + 1);

	if (NULL == out[0])
		goto EXIT_ERROR;

	memcpy(out[0], varible, bytes);
	return  bytes;
EXIT_ERROR:
	perror("\t[ \033[35mString Parse Value\033[0m ] ");
	return -0x1;
}

uint32_t __ServerStringCutOff(char** out, const char* text, const char* cut)
{	
	char* start = NULL;
	uint32_t bytes = 0;

	if (NULL == text) {
		goto EXIT_ERROR;
	}

	/*if (NULL != out[0]) {
		free(*out); 
	}*/


	if (NULL == (start = strstr(text, cut))) {
		*out = (char*)malloc(strlen(text));
		strcpy(*out, text);
		goto EXIT;
	}
	
	while(&start[-1] > *out && start[-1] == ' ') {
		start--;
	}

	/// subracting the bigger memory address with smaller one to get the number of bytes used
	bytes = start - text;
	out[0] = (char*)malloc(bytes + 1);

	if (NULL == *out) {
		goto EXIT_ERROR;
	}

	memset(*out, 0, bytes + 1);
	memcpy(*out, text, bytes);

EXIT:
	return bytes;
EXIT_ERROR:
	*out = NULL;
	return 0x0;
}


__StringHandler StringHandler = {
	.ParseValue		= &__ServerStringParseValue,
	.CutOff			= &__ServerStringCutOff,
};
