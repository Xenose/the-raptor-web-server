#ifndef __server_string_header__
#define __server_string_header__

#include<stdint.h>

typedef struct {
	uint32_t (*Create)(const char* text, char** out);
	uint32_t (*CutOff)(char** out, const char* text, const char* cut);
	uint32_t (*ParseValue)(char** out, const char* text, const char* par);
} __StringHandler;

extern __StringHandler StringHandler;

#endif //__server_string_header__
