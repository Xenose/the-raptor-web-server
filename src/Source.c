#include"Types.h"
#include"Extensions.h"
#include"IO.h"
#include"Server.h"

const char* testIP = NULL;
const char* testPort = "8080";

int sock = 0;

static atomic_int_least8_t dataInUse = 0;

// Note to self Signals are in Signal.h
static void SignalHandler(int sig) {

	if (0 != sock) {
		close(sock);
	}

	switch (sig) {
		case SIGABRT:
			break;
		case SIGINT:
			break;
	}
	
	signal(sig, SIG_DFL);
	raise(sig);
}

int ParseMainArgs(const int* restrict arc, char* restrict* restrict arv, Server* restrict server) {
	return 0;
}

int ThreadLoop(void* ptr) {
	Server* server = (Server*)ptr;
	struct timeval timeout = {};
	fd_set readCopy = {};
	fd_set writeCopy = {};

	timeout.tv_usec = 100;

	while (SERVER_FLAG_EXIT & server->flags) {
		
		if (!dataInUse) {
			dataInUse = 1;
			readCopy = server->readSocks;
			writeCopy = server->writeSocks;
			select(server->maxSocket + 1, &readCopy, &writeCopy, 0, &timeout);


			for (Client* c = server->clients; NULL != c; c = c->pNext) {
				
				// if the client is ready to send ous the data we read it
				if (FD_ISSET(c->socket, &readCopy)) {
					puts("\n\t[ \033[35mReciving\033[0m ]\n");
					ClientHandler.Recv(c);
				}

				if (0 >= c->receivedBytes) {
					break;
				}

				// if the client is ready to recive data then we send it
				if (FD_ISSET(c->socket, &writeCopy)) {
					puts("\n\t[ \033[36mSending\033[0m ]\n");	
					ClientHandler.Send(c, server);
					ClientHandler.Drop(&c, server);
					break;
				}
			}

			// toggling the thread sync so that the other thread can use the data
			dataInUse = 0;
		}
		
		if (SERVER_FLAG_USE_SINGLE_THREAD & server->flags) break;
		usleep(1);
	}

	return 0x0;
}

int Main(int arc, char** arv)
{
	signal(SIGINT, &SignalHandler);
	signal(SIGSEGV, &SignalHandler);

	char ThreadStack[CLONE_STACK_SIZE];
	memset(&ThreadStack, 0, CLONE_STACK_SIZE);
	
	Server server;
	memset(&server, 0, sizeof(Server));
		
	server.pIP					= (char*)testIP;
	server.pPort				= (char*)testPort;
	server.queueSize			= 10;
	server.flags				= SERVER_FLAG_EXIT;// | SERVER_FLAG_USE_SINGLE_THREAD;
	server.extensions.html	= NULL;
	server.files				= NULL; 
	
	Print("\033[2J\033[2;1H\tWellcome to the Raptor Web Server!\n\n");

	if (0 != ServerHandler.Create(&server)) {
		goto EXIT;
	}

	FD_ZERO(&server.readSocks);
	FD_ZERO(&server.writeSocks);
	FD_SET(server.socket, &server.readSocks);
	server.maxSocket = server.socket;

	printf("\n");
	ExtensionHandler.Load(&server.extensions, "extensions");
	printf("\n");
	//printf("\n\tFile count is %d\n\n", CheckPublicFileCount("resources"));
	FileHandler.Load(&server.files, "resources");
	ServerHandler.ParsePages(&server);

	sock = server.socket;
	// Starting a new "thread" so that we can handle incoming connections and communication separate
	if (!(server.flags & SERVER_FLAG_USE_SINGLE_THREAD)) {
		clone(&ThreadLoop, ThreadStack + CLONE_STACK_SIZE, CLONE_IO | CLONE_VM | CLONE_FS |  CLONE_FILES, &server);
	}

	// The incoming connection loop
	while (SERVER_FLAG_EXIT & server.flags) {
		struct sockaddr_storage caddr;
		socklen_t clen = sizeof caddr;
		int32_t csock = ServerHandler.Accept(&server, &caddr, &clen);

		printf("%d\n", csock);

		while (csock) if (!dataInUse) {
			dataInUse = 1;
			// now it should be safe to chance data

			ClientHandler.Add(&server.clients, &csock, &caddr, &clen);
			FD_SET(csock, &server.readSocks);
			FD_SET(csock, &server.writeSocks);
			
			if (csock > server.maxSocket) { 
				server.maxSocket = csock;
			}

			csock = 0;
			// now it is unsafe to chance data
			dataInUse = 0;
			usleep(100);
		}
		if (SERVER_FLAG_USE_SINGLE_THREAD & server.flags) ThreadLoop(&server);
	}

EXIT:
	return 0x0;
}
