#include<stdlib.h>
#include<string.h>
#include"StringList.h"

static int8_t __StringListDelete(StringList** list) {
	return 0;
}
static uint32_t __StringListInsert(StringList** list, const char* text) {
	
	(*list)->pNext = (StringList*)malloc(sizeof(StringList));
	(*list) = (*list)->pNext;
	
	(*list)->bytes = strlen(text);
	(*list)->pData = (char*)malloc(sizeof(char) * (*list)->bytes);
	memcpy((*list)->pData, text, (*list)->bytes);

	return (*list)->bytes;
}

const __StringListHandler StringListHandler = {
	.Delete = &__StringListDelete,
	.Insert = &__StringListInsert,
};
