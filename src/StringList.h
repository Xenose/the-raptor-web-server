#ifndef __string_list_header__
#define __string_list_header__

#include<stdint.h>

typedef struct __StringList {
	char* pData;
	uint32_t bytes;
	struct __StringList* pNext;
} StringList;

typedef struct {
	int8_t (*Delete)(StringList** list);
	uint32_t (*Insert)(StringList** list, const char* text);
} __StringListHandler;

extern const __StringListHandler StringListHandler;

#endif //__string_list_header__
