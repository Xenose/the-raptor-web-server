#include"Types.h"

char* __ServerFileLoadFile(const char* restrict path, uint32_t* restrict bytes) {
	char* out = NULL;
	char* map = NULL;
	int32_t fd = 0;
	
	if (NULL == path)
		return NULL;

	fd = open(path, O_RDONLY);

	*bytes = sizeof(char) * lseek(fd, 0, SEEK_END);
	map = (char*)mmap(NULL, *bytes, PROT_READ, MAP_PRIVATE, fd, 0);
	out = (char*)malloc(*bytes + 1);
	memcpy(out, map, *bytes);

	munmap(map, *bytes);
	close(fd);
	return out;
}

int8_t __ServerFileLoad(ServerFile** files, const char* folder) {
	DIR* ffd = NULL;
	char path[PATH_MAX] = "";
	ServerFile* last = NULL;

	if (NULL == files) {
		goto EXIT_ERROR;
	}
	
	ffd = opendir(folder);

	if (NULL == ffd) {
		perror(" [ \033[31mFAILED\033[0m ]");
		goto EXIT_ERROR;
	}

	if (NULL == *files) {
		files[0] = (ServerFile*)malloc(sizeof(ServerFile));
		files[0]->pNext = NULL;
		files[0]->pData = NULL;
		files[0]->lang = NULL;
	}

	for (last = files[0]; NULL != last->pNext; last = last->pNext) {
	}

	for (struct dirent* i = readdir(ffd); NULL != i; i = readdir(ffd)) {
		memset(path, 0, sizeof path);
		strcpy(path, folder);
		strcpy(path + strlen(path), "/");

		if (DT_REG & i->d_type) {
			strcpy(path + strlen(path), i->d_name);
			strcpy(last->name, path);
			last->pData = __ServerFileLoadFile(last->name, &last->bytes);

			last = last->pNext = (ServerFile*)malloc(sizeof(ServerFile));
			continue;
		}

		if (!(DT_DIR & i->d_type))
			continue;

		if (!strcmp(i->d_name, ".."))
			continue;

		if (!strcmp(i->d_name, "."))
			continue;
			

		strcpy(path + strlen(path), i->d_name);
		__ServerFileLoad(files, path);
	}

EXIT:
	return 0x0;
EXIT_ERROR:
	return -0x1;
}


const ServerFile* __ServerFileGet(const ServerFile* file, const char* name)
{
	ServerFile* out = (ServerFile*)file;

	while (out != NULL) {
		if (!strcmp(name, file->name)) {
			break;
		}

		out = out->pNext;
	}

	return out;
} 

const __FileHandler FileHandler = {
	.LoadFile	= &__ServerFileLoadFile,
	.Load			= &__ServerFileLoad,
	.Get			= &__ServerFileGet,
}; 
