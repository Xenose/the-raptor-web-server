#ifndef __header_types__
#define __header_types__

#define _GNU_SOURCE
#include<arpa/inet.h>
#include<fcntl.h>
#include<dirent.h>
#include<netdb.h>
#include<netinet/in.h>
#include<linux/limits.h>
#include<stdatomic.h>
#include<sched.h>
#include<sys/utsname.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/mman.h>
#include<string.h>
#include<stdio.h>
#include<signal.h>
#include<unistd.h>

#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>

// My headers
#include"List.h"

#define Main main

#define SERVER_THREAD_FLAG_IN_USE	0x01

#define SERVER_FLAG_EXIT						0x01
#define SERVER_FLAG_USE_SINGLE_THREAD		0x02

#define MAX_REQUEST_SIZE 4095
#define CLONE_STACK_SIZE 8096

typedef struct __Extension Extension;
typedef struct __API API;

typedef enum {
	SERVER_FILE_TYPE_HTML,
	SERVER_FILE_TYPE_CSS,
	SERVER_FILE_TYPE_TEXT,
	SERVER_FILE_TYPE_CSV,
	SERVER_FILE_TYPE_GIF,
	SERVER_FILE_TYPE_X_ICON,
	SERVER_FILE_TYPE_JPEG,
	SERVER_FILE_TYPE_JAVASCRIPT,
	SERVER_FILE_TYPE_JSON,
	SERVER_FILE_TYPE_PNG,
	SERVER_FILE_TYPE_PDF,
	SERVER_FILE_TYPE_SVG_XML,
	SERVER_FILE_TYPE_OCTET_STREAM
} ServerFileTypes;

typedef enum {
	EXTENSION_TYPE_HTML
} ExtensionType;


typedef struct __ServerFile {
	char name[PATH_MAX];
	char* lang;
	ServerFileTypes fileType;

	char* pData;
	uint32_t bytes;

	struct __ServerFile* pNext;
} ServerFile;

typedef struct __Client {
	int32_t socket;
	char ip[128];
	char request[MAX_REQUEST_SIZE + 1];
	int32_t receivedBytes;
	
	socklen_t storageLength;
	struct sockaddr_storage storage;
	
	
	struct __Client *pNext;
} Client;


typedef struct {
	Extension* html;
} Extensions;

typedef struct {
	char* pPort;
	char* pIP;

	int32_t socket;
	int32_t maxSocket;
	int32_t queueSize;
	fd_set readSocks;
	fd_set writeSocks;

	Client* clients;
	ServerFile* files;
	char* languages;

	atomic_uint_least32_t flags;
	Extensions extensions;
} Server;

struct __Extension {
	char* name;
	char* type;
	char* path;

	int32_t (*Execute)(API* api);
	Extension* pNext;
};


typedef struct {
	char* (*LoadFile)(const char* restrict path, uint32_t* restrict bytes);
	int8_t (*Load)(ServerFile** files, const char* name);
	const ServerFile* (*Get)(const ServerFile* file, const char* name);
} __FileHandler;

typedef struct __API { 
	char* command;
	char* commandEnd;
	Server* server;
	ServerFile* file;
	StringList* slist;
} API;

extern const __FileHandler FileHandler;

#endif // __header_types__
